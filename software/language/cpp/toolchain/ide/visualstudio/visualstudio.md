Har bir operatsion tizimda odatda dasturiy ta'minot yaratish uchun tavsiya etilgan ish qurollari bo'ladi. Windows operatsion tizimi uchun VisualStudio va u bilan birga o'rnatiladigan kompilyatorlar shunday tavsiya etilgan ish qurollarga kiradi.

Ornatuvchi dasturni www.visualstudio.com sahifasidan yoki ushbu torrent orqali yuklab olishingiz umkin. <br>
O'rnatilish jarayonini birinchi qadamlari tayyorgarlik:

![](/vs_image/p1.png)<br>

O'rnatiladigan komponentlar ro'yxati keyingi oynada keltiriladi. Bulardan bizga keraklisi "Desktop development with C++". O'ng tarafdagi panelda tanlangan elementlar asliligicha qolaversin, kerak bo'lganda istalgan vaqtda ish uchun yetishmaganini o'rnatish mumkin.

![](/vs_image/p4.png)<br>

O'rnatish uchun install tugmasini bosamiz

Keyingi qadamlarda faqat kutishimiz kerak bo'ladi

![](/vs_image/p5.png)<br>

![](/vs_image/p6.png)<br>

**Start Visual Studio** yozuvi ostidagi **Launch** tugmasini bosamiz

![](/vs_image/p7.png)<br>

Visual Studio ishga tushadi va birinchi oynada avtorizatsiyadan o'tish so'raladi. Agar avtorizatsiyadan o'tmasangin Visual Studio cheklangan vaqt davomida ishlaydi keyin avtorizatsiyadan o'tishiz zarur bo'ladi yoki ishlashni davom ettira olmaysiz.

![](/vs_image/p8.png)<br>

Microsoft akauntingiz bo'lsa **Sign in** tugmasi orqali ushbu akauntizga kirishingiz mumkin bo'ladi. Yangi akaunt yaratmoqchi bo'lsangiz **Sign up** havolasi orqali **Microsoft account** sahifasiga o'tishingiz mumkin.
![](/vs_image/p9.png)<br>

Ro'yxatdan o'tganizdan keyin **Visual Studio**da avtorizatsiyadan o'tishingiz mumkin bo'ladi.

![](/vs_image/p11.png)<br>

## Birinchi ishga tushirish<br>
VisualStudio ning asosiy oynasu quyidagi ko'rinishga ega.
![](/vs_image/w1.png)<br>
Yangi loyiha yaratish uchun **Create new project...** havolasini yoki **File** menusidan foydalanish mumkin.
![](/vs_image/w2.png)<br>
Yangi loyiha oynasining chap ismida loyiha turini tanlash mimkin. Bizga ayni vaqtda namoyish uchun **Windows Console Application** yetarli bo'ladi.
![](/vs_image/w4.png)<br>
![](/vs_image/w8.png)<br>
Yangi loyiha yaratilganda C++ ga hos bo'lgan **Hello World!** dastur matnini ko'rishingiz mumkin. 
```cpp
// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

int main()
{
    std::cout << "Hello World!\n"; 
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

```
Lekin bu yerda c++ tili bo'yicha qo'llanma va kitoblarda keltirilmagan bir xususiyatni kuzatishingiz mumkin. Bu #include "pch.h" satri. Ushbu faylda ko'rsatilgan *.h fayllar va ularga tegishli dastur qismlari oldindan kompilyatsiyadan o'tkaziladi va har safar **Build** bajarilganda qayta kompilyatsiya qilinmaydi agar o'zgartirilgan bo'lmasa. Bu imkoniyat yirik katta xajmga ega bo;lgan dasturlarni yog'ilish vaqtini tezlashtiradi. Bu imkoniyatdan GCC va boshqa kompilyatorlardaham foydalanish mumkin.

![](/vs_image/run.png)<br>
Keltirilgan dastur matnini **Local Windows Debuger** tugmasini yoki F5 klaviatura tugmnasi orqali **Debug** (hatolarni qidirish) tartibida va Ctrl+F5 klaviatura tugmalarini bosish orqali **Debug**siz tartibda ishga tushirish mumkin. 

### Debug. Dastur matnidagi hatolarni qidirish.
Ushbu imkoniyatni namoyish qiolish uchun **Hello World!** dastur matniga quyidagi o'zgartirishlarni kiritamiz:
```cpp
#include <iostream>

int main()
{
	int a,b,c;
	a = 1;
	b = 2;
	c = a + b;
    std::cout << "a="<<a<<"; b="<<b<<"; c="<<c; 
}
```
Dastur ishini kuzatish uchun to'xtash nuqtasini belgilaymiz.

![](/vs_image/break_point.png)<br>

Buning uchun sichqonchani chap tugmasini kerakli satr to'g'risidagi maydonga bosamiz. Dasturni F5 tugmasi orqali **Debug** tartibida ishga tushiramiz va biz belgilagan satrda to'xtaganini kuzatishimiz mumkin

![](/vs_image/break_point_stop.png)<br>

Biz hech qanday buyruq bermagunimizcha dastur ishini davom eta olmaydi. Algoritmimizni keyingi qadamga o'tishi uchun klaviaturaning F10 tugmasidan foydalaniladi. Qadamlarni kuzatish bilan birga o'zgaruvchilarimizni qiymatlarini kuzatib borishimiz mumkin

![](/vs_image/debugging.png)<br>

Dasturni qadamlarini F10 tugmasi orqali yoki F5 tugmasi orqali oxiriga yetganimizda bi **Debug** tartibida kuzatgan qiymatlarni natija oynasida ko'rishimiz mumkin. 

Yuqorida **Visual Studio** dasturlash muhiti bilan tanishuv uchun eng asosiy malumotlar keltirildi. Bu muhitning imkoniyatlari juda keng va dasturiy ta'minot yaratish jarayonini loyihalash dan boshlab buyurtmachiga yetkazishgacha bo'lgan bosqichlarini to'liq qamrab oladi.